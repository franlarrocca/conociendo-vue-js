Vue.filter('currency_filter', (value, currency) => {
    return `${currency} ` + `${value}`;
});

//Los filters nos sirven para poder modificar datos
//value es el valor al cual le queremos realizar modificaciones, currency en este caso
//sería el simbolo de moneda a aplicar en el dato amount.
//Linea 21: componentComputedPropertiesGetSet.js