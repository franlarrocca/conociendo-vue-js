Vue.directive('focus', {
    inserted (el) { //el método inserted recibe el elemento al cual le queremos agregar la directiva. (se ejecuta cuando el elemento es insertado en el DOM)
        el.focus();
    }
});

