Vue.component('component-emit', {
    data(){
        return {
            carBrand: 'Ford'
        }
    },
    template: `
        <div>
            <h2>Emitir evento en vue js 2</h2>
            <button v-on:click="$emit('show-car-brand', carBrand)">Click acá para emitir un evento a la instancia root</button>
            <hr/>
       </div>
    
    `
});

//$emit es el método que nos provee Vue para emitir eventos de un componente A a un componente B.
//$emit recibe como primer parámetro el nombre del evento a emitir y luego el dato que le queremos enviar.