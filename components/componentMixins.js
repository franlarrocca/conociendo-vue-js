let myMixin = {
    mounted() {
        console.log("%c" +
            "MIXIN init",
            "color:yellow;");

        console.log("%c" +
            this.mixinData,
            "color:yellow;");

        this.test();
    },
    data() {
        return {
            mixinData: 'Mixin data'
        }
    },
    methods: {
        test() {
            console.log("%c" +
                "test desde mixin",
                "color:yellow;");
        }
    }
};

Vue.component('component-mixins', {
    mixins: [myMixin],
    mounted() {
        console.log("%c" +
            'Mounted desde component-mixins',
            "color:yellow;");
    },
    data() {
        return {
            mixinData: 'Mixin data desde componente'
        }
    },
    template: `
    <div>
        <h2>Uso de mixins</h2>
        <p>{{mixinData}}</p>
        <hr/>
    </div>
       `
});

//Mixins se trata de una funcionalidad de Vue que permite reutilizar cualquier cosa de los componentes 
//para que no tengas que duplicar funcionalidad o no acabes con demasiados componente con demasiados props.