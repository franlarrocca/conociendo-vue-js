Vue.component('component-data-binding-checkbox',{
    data (){
        return {
            frameworks: []
        }
    },
    template: `
        <div>
            <h2>Componente data binding checkbox array (v-model)</h2>
            <input type="checkbox" id="vuejs2" value="vue js 2" v-model="frameworks"/>
            <label for="vuejs2">Vue js 2</label>
            <input type="checkbox" id="reactjs" value="react js" v-model="frameworks"/>
            <label for="reactjs">react js</label>
            <input type="checkbox" id="angular" value="angular" v-model="frameworks"/>
            <label for="angular">angular</label>
            <p>Frameworks seleccionados: {{frameworks}}</p>
            <hr>
       </div>
    
    `
});

//Usando v-model, Vue asigna como valor al elemento del html el mismo valor del atributo definido en el data 
//(con el mismo nombre). Al actualizarse Vue actua como watcher y lo actualiza dinámicamente.