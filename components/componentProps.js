Vue.component('component-props', {
    props: {
        name: {
            type: String,
            required: true
        },
        surename: {
            type: String,
            required: true
        },
        age: {
            type: Number,
            required: true,
            validator: value => {
                if (value < 18) {
                    console.warn('No eres mayor de edad...');
                    return false;
                }
                return true;
            }
        },
    },
    template: `
    <div>
        <h2>Props con vue js 2</h2>
        <p>{{name}} {{surename}} {{age}}</p>
    </div>
    `
});


// Los props dentro del componente en el que se declaran no son más que variables. 
// Como pasa con las variables, los props también son reactivos, es decir, 
// cuando desde fuera el valor del prop cambie, 
// automáticamente se actualizará la vista y las propiedades computadas asociadas a ese prop.

// Los props se declaran dentro de la sección props del componente. 
// <script>
// export default {
//   props: {
//     title: String
//   }
// };
// </script>

// Se declara el nombre del prop y seguido se pone su tipo.

// Los tipos que se pueden declarar son:
//     String: Para cadenas de texto.
//     Number: Para números.
//     Boolean: Para booleanos true/false.
//     Array: Listas ya sea de tipos básicos como de objetos.
//     Object: Objetos de javascript.
//     Date: Tipo fecha de javascript.
//     Function: Funciones.
//     Symbol: Símbolos.

