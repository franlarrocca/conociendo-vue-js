Vue.component('component-load-dynamic-components', {
    data() {
        return {
            components: ['component-one', 'component-two', 'component-three'],
            currentComponent: 'component-one'
        }
    },
    methods: {
        changeComponent(cmp) {
            this.currentComponent = cmp;
        }
    },
    template: `
        <div>
            <h2>Componentes dinámicos</h2>
            <button v-for="cmp in components" :key="cmp" v-on:click="changeComponent(cmp)">Select {{cmp}}</button>
            <component v-bind:is="currentComponent"/>
            <hr/>
        </div>

    `
});