Vue.component('component-loops', {
    data() {
        return {
            frameworks: [
                { id: 1, name: 'Vue js 2' },
                { id: 2, name: 'React js' },
                { id: 3, name: 'Ember' },
                { id: 4, name: 'Angular js' },
                { id: 5, name: 'Laravel' },
                { id: 6, name: 'Adonisjs' },

            ]
        }
    },
    template: `
        <div>
            <h2>Loops con v-for</h2>
            <ul v-if="frameworks.length">
                <li v-for="framework in frameworks" :key="framework.id">
                    {{framework.name}}
                </li>
            </ul>
            <hr/>
        </div>
    `
})