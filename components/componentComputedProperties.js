Vue.component('computed-properties', {
    data() {
        return {
            name: 'Tu nombre',
            surename: 'Tu apellido'
        }
    },
    computed: {
        fullName() {
            return `${this.name} ${this.surename}`
        }
    },
    template: `
        <div>
            <h2>Componente computed properties</h2>
            <p>{{fullName}}</p>
            <hr/>
        </div>
    `
});

//Las computed properties son variables (definidas en forma de función) a las que antes 
//se le pueden aplicar una serie de cálculos o transformaciones. 
//Estas funciones SIEMPRE retornan un valor.

//¿Qué resuelve?
//Las variables que se crean en el data no pueden depender de otras variables del data, 
//por ejemplo no podes crear una variable en el data cuyo valor sea el doble de otra variable. 
//Las propiedades computadas vienen a resolver este problema. 
//Es decir cuando cambia el valor de la variable, se invoca esta función y nos retorna su valor actualizado,
//permitiendo que no tengamos que interpolar una y otra vez el valor de esa variable.

//Diferencias entre method y computed:
//Las propiedades computadas son reactivas y su valor se actualiza solo. Un método que devuelva algo del data 
//solo se ejecutaría la primera vez y no reaccionaria a un cambio en la variable del data.
//Otra diferencia es que las propiedades computadas tienen caché, es decir, 
//utilizar propiedades computadas es más óptimo porque si Vue detecta que la computada va a devolver el mismo valor,
//no ejecutará la computada ahorrando cálculos.
//Los métodos se ejecutan siempre cada vez aunque el resultado sea el mismo.