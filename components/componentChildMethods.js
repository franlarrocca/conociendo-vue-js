Vue.component('component-child-methods', {
    data() {
        return {
            componentName: 'component child methods'
        }
    },
    methods: {
        showComponentName() {
            console.log("%c" +
                'Componente hijo (methods): ' + this.componentName,
                "color:aqua;");
        }
    },
    template: `
        <div>  
            <h2>Acceso a methods del componente hijo</h2>
            <hr/>
        </div>
    
    `
});

//ref nos permite darle un nombre de refrencia a un componente. Para luego poder accederlo. (Linea 57: index.html).
//Una vez ya definida la referencia a nuestro componente podemos accederlo por ejemplo desde el componente padre. 
//Por medio de la funciín mounted la cual pertence al ciclo de vida de un componente.
//mounted nos permite acceder al componente una vez ya montado en la app.
//En dicha función accedemos al componente hijo por medio de componentePadre.$refs.metodoHijo. (Linea 49 a 53: index.html).

