Vue.component('component-watcher', {
    data() {
        return {
            user: null,
            oldUser: null
        }
    },
    methods: {
        async randomUser() {
            try {
                const data = await fetch('https://randomuser.me/api/');
                const json = await data.json();
                const user = json.results[0];
                this.user = `${user.name.title} ${user.name.first} ${user.name.last}`;
            }
            catch (e) {
                console.log("Error en componentWather:", e);
            }
        }
    },
    watch: {
        user(newValue, oldValue) {
            this.user = newValue;
            this.oldUser = oldValue;
        }
    },
    template: `
        <div>
            <h2>Watchers con Vue</h2>
            <button v-on:click="randomUser">Generar un username</button>    
            <p>Nuevo usuario: {{user}}</p>
            <p>Usuario anterior: {{oldUser}}</p>
            <hr/>
        </div>
    `
});

// Cuando se llama a una función async, esta devuelve un elemento Promise. 
// Cuando la función async devuelve un valor, Promise se resolverá con el valor devuelto. 
// Si la función async genera una excepción o algún valor, 
// Promise se rechazará.

// Una función async puede contener una expresión await, 
// la cual pausa la ejecución de la función asíncrona y espera la resolución de la Promise pasada y,
// a continuación, reanuda la ejecución de la función async devolviendo el valor resuelto.

// Watch sirve para vigilar variables que pertenecen a nuestra app. 
// Por ejemplo, si en data tenemos a la propiedad user.
// Definimos a watch como un objeto, y las propiedades de ese objeto son funciones. 
// Cada función debe tener exactamente el mismo nombre que la variable que estamos vigilando.
// Cuando cambie el valor, el framework se encargará de llamar a esa función. 
// Le pasará 2 argumentos, en el siguiente orden: el nuevo valor y el valor anterior.