Vue.component('component-data-binding',{
    data (){
        return {
            framework: 'vue js 2'
        }
    },
    template: `
        <div>
            <h2>Componente data binding (v-model)</h2>
            <input v-model="framework" v-focus/>
            <p>El valor del framework recibido es: {{framework}}</p>
            <hr>
       </div>
    
    `
});

//Usando v-model, Vue asigna como valor al elemento del html el mismo valor del atributo definido en el data 
//(con el mismo nombre). Al actualizarse Vue actua como watcher y lo actualiza dinámicamente.
