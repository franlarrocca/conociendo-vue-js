Vue.component('component-slot',{
    template: `
        <div>
            <h2>Slots, ejemplo layout</h2>
            <header>
                <slot name="header"></slot>
            </header>
            <main>
                <slot></slot>
            </main>
            <footer>
                <slot name="footer"></slot>
            </footer>
            <hr/>
        </div>
    `
});

//Los slots nos sirven para agregar o modificar (sobreescribir) un determinado elemanto del sitio, por ejemplo un header
// un navbar o un footer.