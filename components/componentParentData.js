Vue.component('component-parent-data', {
    template: `
        <div>
            <h2>Acceso a data del componente padre</h2>
            <p>{{$parent.appName}}</p>
            <hr/>
        </div>
    
    `
});

//Con la directiva $parent podemos acceder a atributos y metodos del componente padre.