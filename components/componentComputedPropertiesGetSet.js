Vue.component('component-computed-properties-get-set', {
    data() {
        return {
            amount: 0
        }
    },
    computed: {
        amountFormatted: { // A difrencia de una función (en el objeto computed), se define un objeto con getters y setters.
            get() {
                return `${this.amount}`;
            },
            set(newValue) {
                this.amount = newValue;
            }
        }
    },
    template: `
        <div>
            <h2>Componente computed properties GET / SET</h2>
            <input v-model="amount"/>
            <p>{{amountFormatted | currency_filter('AR$')}}</p>
            <hr/>
        </div>
    `
});

