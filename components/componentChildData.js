Vue.component('component-child-data', {
    data(){
        return {
            componentName: 'component child data'
        }
    },
    template: `
        <div>
            <h2>Acceso a data del componente hijo</h2>
            <hr/>
        </div>
    
    `
});

//ref="referencia" nos permite darle un nombre de refrencia a un componente. Para luego poder accederlo. (Linea 56: index.html).
//Una vez ya definida la referencia a nuestro componente podemos accederlo por ejemplo desde el componente padre. 
//Por medio de la función mounted la cual pertence al ciclo de vida de un componente.
//mounted nos permite acceder al componente una vez ya montado en la app.
//En dicha función accedemos al componente hijo por medio de componentePadre.$refs.atributoHijo. (Linea 102 a 104: index.html).

