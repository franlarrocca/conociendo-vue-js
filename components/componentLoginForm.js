Vue.component('component-login-form', {
    data() {
        return {
            logged: false,
            user: {
                email: '',
                password: ''
            }
        }
    },
    methods: {
        login () {
            this.logged = this.user.email === 'test@test.com' && this.user.password === '12345';
        }
    },
    template: `
        <div>
            <h2>Login form</h2>
            <p v-show="logged" style="background: rgba(29, 187, 108, 0.493);">
                Has iniciado sesión con los datos: {{user}}
            </p>
            <form v-on:submit.prevent="login">
                <input type="email" name="email" v-model="user.email" autocomplete="off">
                <input type="password" name="password" v-model="user.password">
                <button type="submit">Login</button>
            </form>
            <hr/>    
        </div>
    `
});

//v-show se utiliza para mostrar un determinado elemento en el html basándose en determinada condición.


