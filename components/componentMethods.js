Vue.component('component-methods', {
    data() {
        return {
            name: 'Tu nombre',
            surename: 'Tu apellido'
        }
    },
    computed: {
        fullName() {
            return `${this.name} ${this.surename}`
        }
    },
    methods:{
        hello(){
            alert(this.fullName);
        }
    },
    template: `
        <div>
            <h2>Componente methods</h2>
            <button v-on:click="hello">Hace click!</button>
            <hr/>
        </div>
    `
});

//Para bindear un evento podemos usar v-on:evento o @evento en el elemento del html al cual queremos
//realcionar con determinado método en este caso "hello()".
//¿Por que hello sin paréntesis? Vue es muy inteligente y sabe que tiene que ejecutar un método.