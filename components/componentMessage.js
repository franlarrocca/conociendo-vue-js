Vue.component('component-message', { //El primer atributo es el nombre del componente que se va a utilizar luego en el html en forma de tag.
    data() {
        return {
            message: 'Hola mundo'
        }
    },
    template: `
        <div>
            <h2>Componente message</h2>
            <img src="assets/logo.png" alt="" srcset="" width="120" height="100">
            <p>¡{{message}}!</p>
            <hr/>
        </div>
    `
});

//Siempre en el template se debe definir un <div> padre. Ya que en caso contrario nos daría error.