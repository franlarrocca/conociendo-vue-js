const AboutMe = {
    install: (Vue, options) => { //El método install nos permite exponer el plugin.
        const { job } = options;
        Vue.prototype.$me = (name, age) => {
            return `Mi nombre es ${name}, tengo ${age} años y soy ${job}`;
        }
    }
};

Vue.use(AboutMe, {
    job: 'Software developer'
});

// El sistema de plugins de Vue está hecho para que podamos meter funcionalidad de manera global 
// y transversal para ser usada por todos y cada uno de los componentes de nuestro árbol.