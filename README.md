# Conociendo VUE JS
En este proyecto se ven conceptos de introducción a VUE JS con fines de aprendizaje.
Conceptos que vas encontrar en este repo son los siguientes:

- Componentes.
- Propiedades computadas.
- Métodos.
- data binding.
- $emit.
- Acceso a datos de un componente padre desde sus componentes hijos y viceversa.
- Mixins.
- props.
- Plugins.
- Filters.

# ¿Por dónde empezar?
Si usas Visual Studio Code, te recomiendo instalar la extensión live server para poder levantar la app (en este caso se utiliza el CDN de VUE). 
Por otro lado recomiendo la extensión VUE DEV TOOLS en tu navegador para que puedas ver como interactúan los componentes.
